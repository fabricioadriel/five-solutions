# Projeto Integrador - 2º BD

Disciplinas:
 - Engenharia de Software - Profº Giuliano Bertoti
 - Linguagem de Programação e Lab2 - Profª Adriana Jacinto
 - Arquitetura de BD - Profº Emanuel Mineda


<p><h1> Integrantes da Five Solutions: </h1> </p>
<ul>
    <li>Estevão Vidal de Sena - RA: 1460281913014 </li>
    <li>Fabrício Adriel Coelho Freire - RA: 1460281923009 </li>
    <li>Jadson Gabriel dos Santos - RA: 1460481721091 </li>
    <li>Samuel Wesley dos Santos Chagas - 1460281923033 </li>
</ul>

### Story Cards
 1ª Entrega 
  - O gerente do projeto pode visualizar um diagrama Gantt com informações básicas das tarefas que estão sendo desenvolidas em um determinado projeto. Estas informações básicas são: nome da tarefa, duração e interdependência entre tarefas (de forma estática por enquanto).
  - Ao passar mouse sobre cada tarefa, o gerente de projetos pode ver informações detalhadas como a do andamento da tarefa e os recursos (de forma estática por enquanto).
  
# Requisitos do Projeto
 O objetivo desta atividade de requisitos é atender as necessidades do cliente.
 Os requisitos estão divididos em 2 partes: Funcionais e Não-Funcionais
equipe. </p>

<h1> Requisitos Funcionais </h1>
<img src="https://gitlab.com/fabricioadriel/five-solutions/uploads/c76de2e0bcc744d2be7f624ecfb6a272/Requisitos_Funcionais.jpeg"></img>

<p><h1> Requisitos Não Funcionais </h1></p>
<ul>
    <li> <b>Disponibilidade</b> - O sistema deverá ter acesso a todo instante, pois é vital para gerenciamento dos serviços da empresa;
    <li> <b>Usabilidade</b> - Fácil acesso de gráficos, dados e informações pelo usuário;
    <li> <b>Escalabilidade</b> – Deverá ter dois tipos de usuários com as respectivas permissões de acesso ao sistema;
    <li> <b>Segurança</b> – Garantir que nenhum usuário tenha acesso indevido a informações de terceiros;
    <li> <b>Eficiência</b> - O sistema deverá processar vários gráficos simultaneamente, sem travamentos ou erros.
</ul>


Na próxima entrega pretendemos adicionar funções como:

<b> ADICIONAR e EDITAR </b>
- tarefas
- integrantes
- reunião
- carga horaria
- membros (Integrantes)
- horas totais
- dias trabalhados
- projetos

<b> Mostrar </b> 
- Dias trabalhados
- Porcentagem
- Membros
- Atividades
- Valores
- Se o projeto esta dentro do prazo


<b> FILTRAR </b>
- Equipe "Integrantes"
- Individual
- Horas trabalhadas
- Dias
- Projetos

Antes de entregar o primeiro prototipo, fizemos alguns esboços:
[](https://gitlab.com/fabricioadriel/five-solutions/uploads/262c9b3fe0883a698add84c995961f5a/Gantt.xlsx)
[](https://gitlab.com/fabricioadriel/five-solutions/uploads/eef5ca4238d412c857a487071b26953c/primeiratela_01.xlsx)
<br>
<br>
<p><h3> Segue o Primeiro Protótipo :</h3> </p>
Esse foi nosso primeiro protótipo desenvolvido em sala de aula:
https://gitlab.com/fabricioadriel/five-solutions/uploads/595c8b1327a0175838ef6f0fa59409f9/Inicio_Prototipo.html
<br>
<br>
Como pretendemos realizar nossa próxima entrega. Ainda esta no esboço inicial, assim a parte em branco pretendemos adicionar novos recursos baseando nas Heuristicas.
<br>
<img src = "https://gitlab.com/fabricioadriel/five-solutions/uploads/4040bb170b03944e18123908589a1db6/GIT.PNG"> </img>


Link sobre a Primeira entrega: https://youtu.be/HD5iiGf1QgY


### Story Cards
 2ª Entrega
 - Será entregar algo funcional, no qual já é possível adicionar algumas tarefas.
 - Ainda estamos escolhendo uma API
 

 ### Story Cards
 3ª Entrega
 - Definição da API
 - Estrutura do Projeto

Video Entrega 3: https://www.youtube.com/watch?v=1pCEjhyxLIw

  ### Story Cards
 4ª Entrega
 - Desenvolvimento do Banco de Dados
 <br>
 <img src = "https://gitlab.com/fabricioadriel/five-solutions/uploads/e3ceb7e4fac8ea0d37ccb351820d0494/BD.jpeg"> </img>

Video Entrega 4:   https://www.youtube.com/watch?v=_Pgw1jTRvf8