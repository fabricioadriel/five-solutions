var taskData = {
	"data": [
		{ "id": 1, "text": "Reunião", "type": "project", "start_date": "02-04-2019 00:00", "duration": 17, "progress": 0.4, "owner": [{"resource_id":"5", "value": 3}], "parent": 0},
		{ "id": 2, "text": "Projetos", "type": "project", "start_date": "02-04-2019 00:00", "duration": 8, "progress": 0.6, "owner": [{"resource_id":"5", "value": 4}], "parent": "1"},
		{ "id": 3, "text": "Integrantes", "type": "project", "start_date": "11-04-2019 00:00", "duration": 8, "parent": "1", "progress": 0.6, "owner": [{"resource_id":"5", "value": 2}]},
		{ "id": 4, "text": "Trabalho", "type": "project", "start_date": "13-04-2019 00:00", "duration": 5, "parent": "1", "progress": 0.5, "owner": [{"resource_id":"5", "value": 4}], "priority":3},
		{ "id": 5, "text": "API", "type": "task", "start_date": "03-04-2019 00:00", "duration": 7, "parent": "2", "progress": 0.6, "owner": [{"resource_id":"6", "value": 5}], "priority":1},
		{ "id": 6, "text": "Estudos", "type": "task", "start_date": "03-04-2019 00:00", "duration": 7, "parent": "2", "progress": 0.6, "owner": [{"resource_id":"7", "value": 1}], "priority":2},
		
	],
  "links": [

	{ "id": "2", "source": "2", "target": "3", "type": "0" },
	{ "id": "3", "source": "3", "target": "4", "type": "0" },
	{ "id": "7", "source": "8", "target": "9", "type": "0" },
	{ "id": "8", "source": "9", "target": "10", "type": "0" },
	{ "id": "16", "source": "17", "target": "25", "type": "0" },
	{ "id": "17", "source": "18", "target": "19", "type": "0" },
	{ "id": "18", "source": "19", "target": "20", "type": "0" },
	{ "id": "22", "source": "13", "target": "24", "type": "0" },
	{ "id": "23", "source": "25", "target": "18", "type": "0" }

  ]
}